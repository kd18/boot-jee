
package com.gitee.hermer.boot.jee.cache.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

import org.springframework.data.redis.serializer.SerializationException;


public class KryoSerializer implements Serializer {

	private final static Kryo kryo = new Kryo();


	@Override
	public String name() {
		return "kryo";
	}


	@Override
	public Serializable deserialize(byte[] bytes) throws SerializationException {
		if(bytes == null || bytes.length == 0)
			return null;
		Input ois = null;
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			ois = new Input(bais);
			return (Serializable) kryo.readClassAndObject(ois);
		} finally {
			if(ois != null)
				ois.close();
		}
	}

	@Override
	public byte[] serialize(Serializable t) throws SerializationException {
		Output output = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			output = new Output(baos);
			kryo.writeClassAndObject(output, t);
			output.flush();
			return baos.toByteArray();
		}finally{
			if(output != null)
				output.close();
		}
	}
}
