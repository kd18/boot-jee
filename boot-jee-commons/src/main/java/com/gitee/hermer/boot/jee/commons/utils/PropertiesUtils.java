
package com.gitee.hermer.boot.jee.commons.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class PropertiesUtils {

	protected static Log logger = LogFactory.getLog(PropertiesUtils.class);

	/**
	 * 加载Properties
	 * 
	 * @param propertyFileName
	 * @return
	 */
	public static Properties loadProperties(String propertyFileName) {
		Properties prop = new Properties();
		InputStreamReader in = null;
		try {
			ClassLoader loder = Thread.currentThread().getContextClassLoader();

			// 方式1：配置更新需重启JVM
			// in = new InputStreamReader(loder.getResourceAsStream(propertyFileName), "UTF-8");;

			// 方式2：配置更新不需要重启JVM
			URL url = loder.getResource(propertyFileName);
			in = new InputStreamReader(new FileInputStream(url.getPath()), "UTF-8");

			if (in != null) {
				prop.load(in);
			}
		} catch (IOException e) {
			logger.error("load "+propertyFileName+" error!", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					logger.error("close "+propertyFileName+" error!", e);
				}
			}
		}
		return prop;
	}

	/**
	 * 获取配置String
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static String getString(Properties prop, String key) {
		return prop.getProperty(key);
	}

	/**
	 * 获取配置int
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	protected static int getInt(Properties prop, String key) {
		return Integer.parseInt(getString(prop, key));
	}

	public static void main(String[] args) {
		Properties prop = loadProperties("mail.properties");
		System.out.println(getString(prop, "mail.sendNick"));
	}
}
