package com.gitee.hermer.boot.jee.commons.utils;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.serializer.SerializeConfig;

public class JsonUtils {
	
	public static Object toJSON(Object o){
		return JSON.toJSON(o);
	}
	
	public static Object toJSON(Object o,ParserConfig config){
		return JSON.toJSON(o,config);
	}
	public static Object toJSON(Object o,SerializeConfig config){
		return JSON.toJSON(o,config);
	}
	/**
	 * 
	 * @Title: 
	 * @Description: Bean转JSON
	 * @param: @param o
	 * @param: @return       
	 * @date: 2017-9-30 下午1:10:23
	 * @throws
	 */
	public static String toJSONString(Object o){
		return JSON.toJSONString(o);
	}
	/**
	 * 
	 * @Title: 
	 * @Description: Bean转JSON
	 * @param: @param o
	 * @param: @param prettyFormat 是否格式化
	 * @param: @return       
	 * @date: 2017-9-30 下午1:09:52
	 * @throws
	 */
	public static String toJSONString(Object o,Boolean prettyFormat){
		return JSON.toJSONString(o,prettyFormat);
	}
	
	
	
	
	/**
	 * 
	 * @Title: 
	 * @Description: JSON转Bean
	 * @param: @param json
	 * @param: @param clazz
	 * @param: @return       
	 * @date: 2017-9-30 下午1:07:03
	 * @throws
	 */
	public static <T> T fromJSONToBean(String json,Class<T> clazz){
		return JSON.parseObject(json, clazz);
	}
	/**
	 * 
	 * @Title: JSON转BeanList 
	 * @Description: 
	 * @param: @param json
	 * @param: @param clazz
	 * @param: @return       
	 * @date: 2017-9-30 下午1:06:35
	 * @throws
	 */
	public static <T> List<T> fromJSONToListBean(String json,Class<T> clazz){
		return JSON.parseArray(json, clazz);
	}
	
	
	
	
	
	
	
	
	
	

}
