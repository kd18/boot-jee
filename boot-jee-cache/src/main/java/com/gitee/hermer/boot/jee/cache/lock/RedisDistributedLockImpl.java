package com.gitee.hermer.boot.jee.cache.lock;

import com.gitee.hermer.boot.jee.commons.exception.PaiUException.IErrorCode;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;

import redis.clients.jedis.JedisCluster;


public class RedisDistributedLockImpl extends UtilsContext implements DistributedLock{
	
	
	public enum LockErrorCode  implements IErrorCode{
		LOCK_TIME_OUT("L000","分布式锁[%s]超时");
		
		private String msg;
		private String code;
		
		LockErrorCode(String code,String msg){
			this.msg = msg;
			this.code = code;
		}

		@Override
		public String getCode() {
			// TODO Auto-generated method stub
			return code;
		}

		@Override
		public String getMessage() {
			// TODO Auto-generated method stub
			return msg;
		}
		
	}
	
	

	private JedisCluster redis;
	
	private String lockName;
	private String lockValue;
	private long timeOut;
	
	public static final long DEFAULT_TIMEOUT = 8000L; //默认超时时间，毫秒为单位

	
	public RedisDistributedLockImpl(String lockName){
		this(lockName, DEFAULT_TIMEOUT);
	}
	
	public RedisDistributedLockImpl(String lockName, long timeOut){
		this.lockName = lockName;
		this.timeOut = timeOut;
	}
	
	@Override
	public boolean isLock(){
		return redis.get(lockName) != null;
	}
	
	@Override
	public void lock() {
		int count = 0;
		for(;;){
			lockValue = getTime() + "";
			Long result = redis.setnx(lockName, lockValue);
			if(result == 1){
				break;
			}
			if(isTimeOut() && count++ % 20 == 0){//没20次检测一次
				redis.del(lockName);
				throw new DistributedLockException(LockErrorCode.LOCK_TIME_OUT,timeOut+"");
			}
			try {
				debug("DistributedLock...");
				Thread.sleep(3L);
			} catch (InterruptedException e) {
				error(e.getMessage(),e);
			}
		}
	}

	@Override
	public boolean tryLock() {
		lockValue = getTime() + "";
		Long result = redis.setnx(lockName, lockValue);
		if(result == 1){
			return true;
		}
		if(isTimeOut()){
			redis.del(lockName);
		}
		return false;
	}

	@Override
	public boolean tryLock(int timeOut) {
		boolean flag = false;
		Long time = System.currentTimeMillis();
		int count = 0;
		for(;;){
			lockValue = getTime() + "";
			Long result = redis.setnx(lockName, lockValue);
			if(result == 1){
				flag = true;
				break;
			}
			if(isTimeOut() && count++ % 20 == 0){//没20次检测一次

				redis.del(lockName);
			}
			try {
				Thread.sleep(3L);
			} catch (InterruptedException e) {
				error(e.getMessage(),e);
			}
			Long now = System.currentTimeMillis();
			if((now - time) >= timeOut){
				break;
			}
		}
		return flag;
	}

	@Override
	public void unLock() {
		String oldValue = redis.get(lockName);
		if(lockValue.equals(oldValue)){
			redis.del(lockName);
		}
	}
	
	private boolean isTimeOut(){
		String oldValue = redis.get(lockName);
		if(oldValue == null){
			return false;
		}
		long now = System.currentTimeMillis();
		long useTime = now - Long.parseLong(oldValue);
		if(useTime >= timeOut){
			return true;
		}
		return false;
	}
	
	/**
	 * 获取当前时间戳，如果无法做到所有的机器时间同步，
	 * 可以采用一个机器提供时间服务，这里调用一个时间服务保证所有机器时钟同步
	 * @return 时间戳
	 */
	private long getTime(){
		return System.currentTimeMillis();
	}

	public void setRedis(JedisCluster redis) {
		this.redis = redis;
	}


}
